# Rename and move media

## About
To rename photos and move to appropiate folders based on year and month.

## Warning
This will rename any and all photos ending in `['.jpg', '.jpeg', '.png','.PNG','.JPG']`

## Limitations
Currently only handles photos. This does not work with video. Recursively goes through all folders in the path you provided to search from.

### Tested Platform
1. MacOS 12.5 M1 - Python 3.9.13

## How to use
1. `pip install -r requirements.txt`
2. You will need two folders. 
    1. The first is where you want to store all your photos. This would be something like `/home/ubuntu/photos`. 
    2. The second place is where you want to search from. This could be where you currently have photos stored or if you just moved them off your camera to `/home/ubuntu/downloads` for example.

## Output

You will see the renaming of all the photos and then where they are being moved to. If there are any media that it can not currently handle, it will print out which fails. Currently video formats are not supported. 
