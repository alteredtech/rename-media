import os
import re
import shutil
import datetime
from PIL import Image
from PIL.ExifTags import TAGS

extensions = (['.jpg', '.jpeg', '.png','.PNG','.JPG'])

def createDirectory(file,parentPath):
    result = re.search("([0-9]{4})-([0-9]{2})",file)
    yearDir = result.group(1)
    monthDir = result.group(2)
    yearMonthPath = os.path.join(yearDir,monthDir)
    path = os.path.join(parentPath,yearMonthPath)
    os.makedirs(path, exist_ok=True)
    return path

def cycleDirectories(searchPath,rootDirectory):
    directoryList = os.listdir(searchPath)
    retry = True
    while retry:
        for file in directoryList:
            filename, extension = os.path.splitext(file)
            # move through the directories until you find files
            if not file.isdigit() and extension=='' and file[:1]!='.':
                cycleDirectories(os.path.join(searchPath,file),rootDirectory)
            # found files with extensions
            elif extension in extensions:
                # try creating directory and moving file
                # if it fails regex file is printed for user
                try:
                    destPath = createDirectory(file,rootDirectory)
                    srcPath = os.path.join(searchPath,file)
                    shutil.copy(srcPath,destPath)
                    print(srcPath.rjust(45) + '    =>    ' + os.path.join(destPath,file).ljust(45))
                    global count
                    count = count + 1 
                    retry = False
                except:
                    print("\n\n")
                    directoryList = changePhotoName(searchPath)
                    break
            else:
                global notMoved
                notMoved.append(file)
    # return out of recursive function
    return

def changePhotoName(directory):
    newFilesList = []
    countRename = 0
    newfilesDictionary = {}
    filelist = os.listdir( directory )
    for file in filelist:
        filename, extension = os.path.splitext(file)
        if ( extension in extensions ):
            try:
                img = Image.open(os.path.join(directory, file))
            except:
                print("could not open %s" % file)
                if os.path.getsize(os.path.join(directory,file)) < 1:
                    print("ommiting %s as it is less than 1 byte" % file)
                    continue
            exif_table={}
            for k, v in img.getexif().items():
                tag=TAGS.get(k)
                exif_table[tag]=v
            if 'DateTime' in exif_table: 
                init_time = exif_table['DateTime']
                format_time_string = init_time.replace(':','-').replace(' ','_')
                newfile = format_time_string + extension;
            else:
                create_time = os.stat(os.path.join(directory, file) ).st_birthtime
                format_time = datetime.datetime.fromtimestamp( create_time )
                format_time_string = format_time.strftime("%Y-%m-%d_%H.%M.%S")
                newfile = format_time_string + extension;

            if ( newfile in newfilesDictionary.keys() ):
                index = newfilesDictionary[newfile] + 1;
                newfilesDictionary[newfile] = index;
                newfile = format_time_string + '--' + str(index) + extension;
            else:
                newfilesDictionary[newfile] = 0;

            os.rename( os.path.join(directory, file), os.path.join(directory, newfile))
            countRename = countRename + 1
            print( file.rjust(45) + '    =>    ' + newfile.ljust(45) )
            newFilesList.append(newfile)
    return newFilesList

if __name__ == "__main__":
    count = 0
    notMoved = []
    rootPath = input("Enter the root path that the photos will be moved to: ")
    while not os.path.exists(rootPath):
        print("\nError: Sorry %s does not exist. Please make sure you can access the path or that the path exists." % rootPath)
        rootPath = input("Enter the root path that the photos will be moved to: ")
    searchPath = input("\nEnter the directory to start searching from: ")
    while not os.path.exists(searchPath):
        print("\nError: Sorry %s does not exist. Please make sure you can access the path or that the path exists." % rootPath)
        searchPath = input("Enter the directory to start searching from: ")
    cycleDirectories(searchPath,rootPath)
    print( 'All done. ' + str(count) + ' files are moved. ')
    for failed in notMoved:
        print("Failed to move: %s" % failed)
